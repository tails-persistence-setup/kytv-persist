#!perl

use strict;
use warnings;

use lib "lib";

use Test::More;
use Test::BDD::Cucumber::StepFile;
use Method::Signatures;

use List::Util qw{first};
use List::MoreUtils qw{all};
use Path::Class;
use File::Temp qw{tempfile};

use Tails::Persistence::Configuration;
use Tails::Persistence::Configuration::File;
use Tails::Persistence::Step::Configure;

sub get_temp_file {
    my ($fh, $filename) = tempfile();
    return file($filename);
}

Given qr{^the file does not exist$}, func($c) {
    my $config_path = get_temp_file();
    $c->{stash}->{scenario}->{config_path} = $config_path;
    ok(defined($config_path));
};

Given qr{^the file is empty$}, func($c) {
    my $config_path = get_temp_file();
    $c->{stash}->{scenario}->{config_path} = $config_path;
    $config_path->touch;
    ok(-e $config_path);
};

Given qr{^the file has a valid one-column line$}, func($c) {
    my $config_path = get_temp_file();
    $c->{stash}->{scenario}->{config_path} = $config_path;
    my $fh = $config_path->openw;
    say $fh "/home/amnesia";
};

Given qr{^the file has only a commented-out line$}, func($c) {
    my $config_path = get_temp_file();
    $c->{stash}->{scenario}->{config_path} = $config_path;
    my $fh = $config_path->openw;
    say $fh "  #/home/amnesia";
};

Given qr{^the file has a valid two-different-columns line$}, func($c) {
    my $config_path = get_temp_file();
    $c->{stash}->{scenario}->{config_path} = $config_path;
    my $fh = $config_path->openw;
    say $fh "/home/amnesia /something/else";
};

Given qr{^the file has two valid two-columns lines$}, func($c) {
    my $config_path = get_temp_file();
    $c->{stash}->{scenario}->{config_path} = $config_path;
    my $fh = $config_path->openw;
    say $fh "/home/amnesia /something/else";
    say $fh "/var/lib/tor /var/lib/tor";
};

Given qr{^the file has a valid line with options '([^']*)'$}, func($c) {
    my $options = $c->matches->[0];
    my $config_path = get_temp_file();
    $c->{stash}->{scenario}->{config_path} = $config_path;
    my $fh = $config_path->openw;
    say $fh "/home/amnesia $options";
};

Given qr{^the file has the following content$}, func($c) {
    my $content = $c->data;
    my $config_path = get_temp_file();
    $c->{stash}->{scenario}->{config_path} = $config_path;
    my $fh = $config_path->openw;
    say $fh $content;
};

Given qr{^I have a Configuration object$}, func($c) {
    my $config_path = get_temp_file();
    $c->{stash}->{scenario}->{config_path} = $config_path;
    $config_path->touch;
    my $configuration = Tails::Persistence::Configuration->new(
        config_file_path => $config_path
    );
    $c->{stash}->{scenario}->{configuration} = $configuration;
    ok(defined($configuration));
};

Given qr{^I have a Step::Configure object$}, func($c) {
    my $configure = Tails::Persistence::Step::Configure->new(
        name => 'configure',
        configuration => $c->{stash}->{scenario}->{configuration},
        device_model => 'device model',
        device_vendor => 'device vendor',
        go_callback => sub { $c->{stash}->{scenario}->{configuration}->save; },
        success_callback => sub { return 1; },
        persistence_partition_device_file => 'persistence partition device file',
        persistence_partition_size => 12000,
    );
    $c->{stash}->{scenario}->{configure} = $configure;
    ok(defined($configure));
};

When qr{^I create a ConfigFile object$}, func($c) {
    my $config_path = $c->{stash}->{scenario}->{config_path};
    my $config_file = Tails::Persistence::Configuration::File->new(path => $config_path);
    $c->{stash}->{scenario}->{config_file} = $config_file;
    ok(defined($config_file));
};

When qr{^I write in-memory configuration to file$}, func($c) {
    $c->{stash}->{scenario}->{config_file}->save;
    ok("in-memory configuration saved to file");
};

When qr{^I merge the presets and the file$}, func($c) {
    my $config_path = $c->{stash}->{scenario}->{config_path};
    my $configuration = Tails::Persistence::Configuration->new(
        config_file_path => $config_path
    );
    $c->{stash}->{scenario}->{configuration} = $configuration;
    ok(defined($configuration));
};

When qr{^I toggle an inactive button on$}, func($c) {
    my $button = first {
        ! $_->is_active
    } $c->{stash}->{scenario}->{configure}->all_buttons;
    $button->set_active(1);
    ok($button->is_active);
};

When qr{^I toggle an active button off$}, func($c) {
    my $button = first {
        $_->is_active
    } $c->{stash}->{scenario}->{configure}->all_buttons;
    $button->set_active(0);
    ok(! $button->is_active);
};

When qr{^I click the save button$}, func($c) {
    $c->{stash}->{scenario}->{configure}->go_button->clicked;
};

Then qr{^the file should be created$}, func($c) {
    ok(-e $c->{stash}->{scenario}->{config_file}->path->stringify);
};

Then qr{^the list of lines in the file object should be empty$}, func($c) {
    is($c->{stash}->{scenario}->{config_file}->all_lines, 0);
};

Then qr{^the list of options should be empty$}, func($c) {
    my @lines = $c->{stash}->{scenario}->{config_file}->all_lines;
    my $line = $lines[0];
    is($line->all_options, 0);
};

Then qr{^the output string should contain (\d+) lines$}, func($c) {
    my $expected_lines = $c->matches->[0];
    my $output = $c->{stash}->{scenario}->{config_file}->output;
    chomp $output;
    my $lines = split(/\n/, $output);
    is($lines, $expected_lines);
};

Then qr{^the file should contain (\d+) line[s]?$}, func($c) {
    my $expected_lines = $c->matches->[0];
    my $config_path = $c->{stash}->{scenario}->{config_path};
    my @lines = file($config_path)->slurp;
    is(@lines, $expected_lines);
};

Then qr{^the file should contain the "([^"]*)" line$}, func($c) {
    my $expected_line = $c->matches->[0];
    my $config_path = $c->{stash}->{scenario}->{configuration}->config_file_path;
    my $matching_lines = grep {
        $_ eq $expected_line
    } file($config_path)->slurp(chomp => 1);
    is($matching_lines, 1);
};

Then qr{^the first line in file should have options '([^']*)'$}, func($c) {
    my $expected_options = $c->matches->[0];
    my $config_path = $c->{stash}->{scenario}->{config_path};
    my @lines = file($config_path)->slurp;
    my $first_line = $lines[0];
    my ($destination, $options) = split /\s+/, $first_line;
    ok(defined($options)
        && length($options)
        && $options eq $expected_options
    );
};

Then qr/^the options list should contain (\d+) element[s]?$/, func($c) {
    my $expected_elements_count = $c->matches->[0];
    my @lines = $c->{stash}->{scenario}->{config_file}->all_lines;
    my $line = $lines[0];
    is($line->count_options, $expected_elements_count);
};

Then qr/^'([^']*)' should be part of the options list$/, func($c) {
    my $expected_option = $c->matches->[0];
    my @lines = $c->{stash}->{scenario}->{config_file}->all_lines;
    my $line = $lines[0];
    is($line->grep_options(sub { $_ eq $expected_option }), 1);
};

Then qr{^the list of configuration atoms should contain (\d+) elements$}, func($c) {
    my $expected = $c->matches->[0];
    is(scalar($c->{stash}->{scenario}->{configuration}->all_atoms), $expected);
};

Then qr{^the list of item buttons should contain (\d+) elements$}, func($c) {
    my $expected = $c->matches->[0];
    is(scalar($c->{stash}->{scenario}->{configure}->all_buttons), $expected);
};

Then qr{^there should be (\d+) enabled configuration lines?$}, func($c) {
    my $expected = $c->matches->[0];
    is($c->{stash}->{scenario}->{configuration}->all_enabled_lines, $expected);
};

Then qr{^I should have a defined Step::Configure object$}, func($c) {
    my $configure = $c->{stash}->{scenario}->{configure};
    ok(defined($configure));
};

Then qr{^there should be (\d+) active button[s]?$}, func($c) {
    my $expected = $c->matches->[0];
    my $nb_active_buttons = grep {
        $_->is_active
    } $c->{stash}->{scenario}->{configure}->all_buttons;
    is($nb_active_buttons, $expected);
};

Then qr{^every active button's atom should be enabled$}, func($c) {
    my @active_buttons = grep {
        $_->is_active
    } $c->{stash}->{scenario}->{configure}->all_buttons;
    ok(all { $_->atom->enabled } @active_buttons);
};

Then qr{^every inactive button's atom should be disabled$}, func($c) {
    my @inactive_buttons = grep {
        ! $_->is_active
    } $c->{stash}->{scenario}->{configure}->all_buttons;
    ok(all { ! $_->atom->enabled } @inactive_buttons);
};

Then qr{^the list box should have (\d+) child(?:ren)?$}, func($c) {
    my @children = $c->{stash}->{scenario}->{configure}->list_box->get_children;
    is(
        @children,
        $c->matches->[0]
    );
};

Then qr{^every active button's checked icon should be displayed$}, func($c) {
    my @active_buttons = grep {
        $_->is_active
    } $c->{stash}->{scenario}->{configure}->all_buttons;
    ok(all { $_->checked_img->get_visible } @active_buttons);
};

Then qr{^every inactive button's checked icon should be hidden$}, func($c) {
    my @inactive_buttons = grep {
        ! $_->is_active
    } $c->{stash}->{scenario}->{configure}->all_buttons;
    ok(all { ! $_->checked_img->get_visible } @inactive_buttons);
};
