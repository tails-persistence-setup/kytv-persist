=head1 NAME

Tails::Persistence::Configuration::Line - read, parse and write live-persistence.conf lines

=cut

package Tails::Persistence::Configuration::Line;

use Moose;
with 'Tails::Persistence::Role::ConfigurationLine';



no Moose;
1;
