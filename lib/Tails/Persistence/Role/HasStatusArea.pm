=head1 NAME

Tails::Persistence::Role::HasStatusArea - status area interface

=cut

package Tails::Persistence::Role::HasStatusArea;
use Moose::Role;



requires 'status_area';
requires 'working';

no Moose 'Role';
1;
